class Environment:
    def __init__(self, old_token, new_token, new_token_dispatcher, tokens):
        self.old_token = old_token
        self.new_token = new_token
        self.new_token_dispatcher = new_token_dispatcher
        self.tokens = tokens

ethereum = Environment(
    old_token = "???",
    new_token = "???",
    new_token_dispatcher = "???",
    tokens = [
        21562,
        63290,
        90176
    ]
)

goerli = Environment(
    old_token = "0x3B3ee1931Dc30C1957379FAc9aba94D1C48a5405",
    new_token = "0x1d022f9f1b8bd67cc1d527d56b0416b916c63130",
    new_token_dispatcher = "0x47Ded080A4EF933e96E357d468363A8A0cac03f0",
    tokens = [
        21562,
        63290,
        90176
    ]
)

local = Environment(
    old_token = None,
    new_token = None,
    new_token_dispatcher = None,
    tokens = ethereum.tokens
)
