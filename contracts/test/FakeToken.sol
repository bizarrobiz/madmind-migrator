pragma solidity ^0.8.0;

// THIS CONTRACT IS FOR TESTING PURPOSES AND IS NOT PART OF THE PROJECT

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract FakeToken is ERC721, Ownable {

    constructor() ERC721("FakeToken", "FAKE") {}

    function mint(address to, uint256 id) public onlyOwner {
        _mint(to, id);
    }

    function mintBatch(address to, uint256[] calldata ids) public onlyOwner {
        for (uint256 i = 0; i < ids.length; ++i) {
            _mint(to, ids[i]);
        }
    }
}
