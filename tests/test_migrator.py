import brownie
from pytest import fixture
from brownie import accounts
from scripts import deploy, environment

BURN_ADDRESS = "0x000000000000000000000000000000000000dEaD"

env = environment.local

@fixture(scope="module", autouse=True)
def deployment():
    oldToken, newToken, migrator = deploy.local()
    yield oldToken, newToken, migrator

@fixture(autouse=True)
def isolation(fn_isolation):
    pass

def test_onERC721Received(deployment):
    oldToken, newToken, migrator = deployment
    new_token_id = 1
    old_token_id = env.tokens[new_token_id - 1]
    assert oldToken.ownerOf(old_token_id) == accounts[0]
    assert newToken.ownerOf(new_token_id) != accounts[0]
    oldToken.safeTransferFrom(accounts[0], migrator, old_token_id, {"from": accounts[0]})
    assert oldToken.ownerOf(old_token_id) == BURN_ADDRESS
    assert newToken.ownerOf(new_token_id) == accounts[0]

def test_onERC721Received_fail_with_different_token(deployment):
    oldToken, newToken, migrator = deployment
    id = 1
    other = deploy.fake_token([id], owner=accounts[0])
    with brownie.reverts("IERC721Receiver: Invalid caller"):
        other.safeTransferFrom(accounts[0], migrator, id, {"from": accounts[0]})

def test_onlyOwner_recoverLostToken(deployment):
    oldToken, newToken, migrator = deployment
    new_token_id = 2
    old_token_id = env.tokens[new_token_id - 1]
    assert oldToken.ownerOf(old_token_id) == accounts[0]
    assert newToken.ownerOf(new_token_id) != accounts[0]
    # Accidentally calls transferFrom instead of safeTransferFrom
    oldToken.transferFrom(accounts[0], migrator, old_token_id, {"from": accounts[0]})
    # Migrator owner recovers the token and completes the migration
    assert migrator.owner() == accounts[0]
    migrator.onlyOwner_recoverLostToken(accounts[0], old_token_id, {"from": accounts[0]})
    assert oldToken.ownerOf(old_token_id) == BURN_ADDRESS
    assert newToken.ownerOf(new_token_id) == accounts[0]

def test_onlyOwner_recoverLostToken_fail_if_not_owner(deployment):
    oldToken, newToken, migrator = deployment
    assert migrator.owner() != accounts[1]
    with brownie.reverts("Ownable: caller is not the owner"):
        migrator.onlyOwner_recoverLostToken(accounts[0], 1, {"from": accounts[1]})
