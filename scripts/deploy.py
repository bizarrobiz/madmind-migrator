from brownie import TokenMigrator, FakeToken, Contract, accounts
from scripts import environment

def local():
    old_token_ids = environment.local.tokens
    new_token_dispatcher = accounts[1]
    oldToken = fake_token(old_token_ids, owner=accounts[0])
    newToken = fake_token(list(range(1, len(old_token_ids) + 1)), owner=new_token_dispatcher)
    migrator = TokenMigrator.deploy(
        oldToken,
        newToken,
        new_token_dispatcher,
        old_token_ids,
        {"from": accounts[0]}
    )
    newToken.setApprovalForAll(migrator, True, {"from": new_token_dispatcher})
    return oldToken, newToken, migrator

def _deploy(env, publish=False):
    old_token_ids = list(map(lambda t: t.old_token_id, env.tokens))
    oldToken = FakeToken.at(env.old_token)
    newToken = FakeToken.at(env.new_token)
    migrator = TokenMigrator.deploy(
        oldToken,
        newToken,
        env.new_token_dispatcher,
        old_token_ids,
        {"from": accounts[0]},
        publish_source=publish
    )
    return oldToken, newToken, migrator

def fake_token(tokenIds, owner, publish=False):
    oldToken = FakeToken.deploy({"from": accounts[0]}, publish_source=publish)
    oldToken.mintBatch(owner, tokenIds, {"from": accounts[0]})
    assert oldToken.ownerOf(tokenIds[0]) == owner
    return oldToken
